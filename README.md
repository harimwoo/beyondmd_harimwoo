# BeyondMD_HarimWoo
---
## BeyondMD Application for Harim Woo

Thank you for your consideration! Please feel free to reach out to me via [LinkedIn](https://www.linkedin.com/in/harimwoo/) for any questions regarding my application.

---

### Application Initialization
1. Clone the repository to your local machine
2. `cd` into the application directory
3. Run `docker compose build`
4. Run `docker compose up`

**CAVEAT:**
5. ctrl + c and spin up docker again with `docker compose up`

### Server
- Prompt_001 runs on `http://localhost:8001/`
- Prompt_002 runs on `http://localhost:8002/`
- Prompt_003 runs on `http://localhost:8003/`

---

### Prompt_003 Design
- New patient can be created. (Full CRUD)
- A new treatment record can be created for each patient. (Create / Delete)
- A random article from HealthCare.gov can be fetched.

### Prompt_003 Data Model

---

### Patient

| Field Name     | Type              | Unique | Optional |
| -------------- | ----------------- | ------ | -------- |
| name           | CharField(255)    | No     | No       |
| age            | PositiveInt       | No     | No       |
| state          | CharField(100)    | No     | No       |
| phone_number   | CharField(15)     | No     | No       |

### Medical History
| Field Name     | Type              | Unique | Optional |
| -------------- | ----------------- | ------ | -------- |
| patient        | ForeignKey(Patient) | -   | -        |
| treatment_name | CharField(255)    | No     | No       |
| doctor_name    | CharField(255)    | No     | No       |
| hospital_name  | CharField(255)    | No     | No       |
| cost           | DecimalField(10, 2) | No   | No       |
| medication     | CharField(255)    | No     | No       |
| treatment_date | DateTimeField()   | No     | No       |
