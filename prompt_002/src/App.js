import logo from './logo.svg';
//import mui components
import { Container, Typography, Box, Paper } from "@mui/material"
//import necessary components
import Greeting from './Components/Greeting'
import Resume from './Components/Resume';
import PatientInfo from './Components/PatientInfo';

function App() {
  return (
    <Container>
      <Typography
        variant="h4"
        sx={{ my: 6, textAlign: "center", color: "#2196f3"}}
      >
        <Greeting />
      </Typography>
      <Paper elevation={23} sx={{bgcolor:"#e3f2fd"}}>
        <PatientInfo />
      </Paper>
      <Paper elevation={23} sx={{bgcolor:"#e3f2fd"}}>
        <Resume />
      </Paper>
    </Container>

  );
}

export default App;
