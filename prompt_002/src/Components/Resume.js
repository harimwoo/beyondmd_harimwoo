import React from "react";

//Create a Resume component
function Resume(){
    return (
        <div>
            <h1>Resume PDF</h1>
            <iframe title="Resume" src="/resume.pdf" width="100%" height="800px"></iframe>
        </div>
    )
}

//Export the Resume component
export default Resume;
