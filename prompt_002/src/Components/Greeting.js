import React from 'react';

//Create Greeting component
function Greeting() {
    return (
        <div>
            <h1>Hello BeyondMD!</h1>
        </div>
    );
}

//Export Greeting component
export default Greeting;
