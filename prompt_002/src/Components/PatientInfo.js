//import React hooks for storing data from API calls
import React, { useState, useEffect } from 'react';

//create PatientData component
function PatientInfo() {
    const [patientInfo, setPatientInfo] = useState([]);

    useEffect(() => {
        const fetchData = async() => {
            // fetches three patient data as it says in the parameter
            const url = 'https://randomuser.me/api/?results=3';
            const response = await fetch(url);
            // if response is ok, parse the response and set it to patientInfo
            if (response.ok){
                const data = await response.json()
                setPatientInfo(data.results)
            } else {
                console.error("Error fetching patient info: ", await response.text());
            }
        }
        fetchData();
    }, [])

    return (
    <div>
        <h2>Patient Info</h2>
        <table>
            <thead>
            <tr>
                <th>Name</th>
                <th>email</th>
                <th>gender</th>
                <th>Age</th>
                <th>dob</th>
                <th>nationality</th>
                <th>Location</th>
                <th>Photo</th>
            </tr>
            </thead>
            <tbody>
            {patientInfo.map((patient, index) => (
              <tr key={index}>
                <td>{`${patient.name.title}. ${patient.name.first} ${patient.name.last}`}</td>
                <td>{patient.email}</td>
                <td>{patient.gender}</td>
                <td>{patient.dob.age}</td>
                <td>{new Date(patient.dob.date).toLocaleDateString()}</td>
                <td>{patient.nat}</td>
                <td>{`${patient.location.city}, ${patient.location.state}, ${patient.location.country}`}</td>
                <td>
                  <img
                    src={patient.picture.large}
                    alt={`${patient.name.first}'s photo`}
                    width="100"
                    height="100"
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
    </div>
    );
  }

//export PatientInfo component
export default PatientInfo;
