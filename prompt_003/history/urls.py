from django.urls import path
# import view functions
from history.views import (
    create_patient,
    list_patients,
    create_treatment,
    show_patient,
    update_patient,
    delete_patient,
    delete_treatment,
    random_article,
)

# urlpatterns for this application !! It is registerd on project's path, so all paths here start with /patients.
# For instance, "create/" will be at `localhost:8003/patients/create/`
urlpatterns = [
    path("", list_patients, name="show_patients"),
    path("create/", create_patient, name="create_patient"),
    path("treatment/create/<int:patient_id>/", create_treatment, name="create_treatment"),
    path("<int:id>/", show_patient, name="detail_patient"),
    path("<int:id>/update/", update_patient, name="update_patient"),
    path("<int:id>/delete/", delete_patient, name="delete_patient"),
    path("delete_treatment/<int:id>/", delete_treatment, name="delete_treatment"),
    path("random_article/", random_article, name="random_article")
]
