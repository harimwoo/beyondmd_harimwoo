from django.shortcuts import render, redirect, get_object_or_404
from history.models import Patient, MedicalHistory
from history.forms import PatientForm, MedicalHistoryForm
import requests, json
import random

# View function for creating a new patient !!
def create_patient(request):
    if request.method == "POST":
        form = PatientForm(request.POST) # using awesome django form !!
        if form.is_valid():
            form.save() # save the form !!
            return redirect("show_patients") # redirect to patient list page !!
    else:
        form = PatientForm()
    context = {"form": form} # assigning obj to context !!
    return render(request, "patients/create_patient.html", context) # feeding the context to HTML template !!

# View function for creating a new treatment !!
def create_treatment(request, patient_id):
    patient = get_object_or_404(Patient, id=patient_id)

    if request.method == "POST":
        form = MedicalHistoryForm(request.POST, initial={"patient": patient})
        if form.is_valid():
            form.save()
            return redirect("detail_patient", id=patient_id)
    else:
        form = MedicalHistoryForm(initial={"patient": patient})
    context = {"form": form}
    return render(request, "treatments/create_treatment.html", context)

# View function for listing patients !!
def list_patients(request):
    patients = Patient.objects.all()
    context = {"patients": patients}
    return render(request, "patients/list_patients.html", context)

# View function for detail patient page !!
def show_patient(request, id):
    patient = get_object_or_404(Patient, id=id) # grab the patient by id !!
    treatments = MedicalHistory.objects.filter(patient=id) # grab all treatments by patient_id !!
    context = {
        "patient": patient,
        "treatments": treatments
               }
    return render(request, "patients/detail_patient.html", context)


# View function for updating patient !!
def update_patient(request, id):
    patient = get_object_or_404(Patient, id=id)

    if request.method == "POST":
        form = PatientForm(request.POST, instance=patient)
        if form.is_valid():
            form.save()
            return redirect("detail_patient", id=id)
    else:
        form = PatientForm(instance=patient)

    context = {"form": form, "patient": patient}
    return render(request, "patients/update_patient.html", context)


# View function for deleting patient !!
def delete_patient(request, id):
    patient = get_object_or_404(Patient, id=id)

    if request.method == "POST":
        patient.delete()
        return redirect("show_patients")

    context = {"patient": patient}
    return render(request, "patients/delete_patient.html", context)


# View function for deleting treatment !!
def delete_treatment(request, id):
    # Retrieve the treatment object !!
    treatment = get_object_or_404(MedicalHistory, id=id)

    if request.method == "POST":
        # Delete the treatment from the database !!
        treatment.delete()
        # Redirect to the patient's detail page after deletion !!
        return redirect("detail_patient", id=treatment.patient.id)
    else:
        pass

# View function for getting a random article !!
def random_article(request):
    url = 'https://www.healthcare.gov/api/articles.json'

    response = requests.get(url)

    if response.status_code == 200:
        try:
            data = response.json()
            articles = data.get('articles', [])

            # Check if it returned at least 5 articles
            if len(articles) >= 5:
                random_article = random.choice(articles)
            else:
                random_article = None
        except json.JSONDecodeError as e:
            print(f"JSON decoding error: {str(e)}")
            random_article = None
    else:
        random_article = None

    context = {"random_article": random_article}
    return render(request, "healtharticle/random_article.html", context)
